package calculator;

import java.util.Arrays;

public class EvenNumbersCalculator {
	public static int add(int[] numbersArray) {
	    return Arrays.stream(numbersArray)
					.filter(n -> n % 2 == 0 && n > 0)
					.sum();
    }
}
