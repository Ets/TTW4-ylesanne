package calculatortest;

import org.junit.Assert;
import org.junit.Test;

import calculator.EvenNumbersCalculator;

public class EvenNumbersCalculatorTest {
	@Test
	public final void emptyArrayReturnsZero() {
		Assert.assertEquals(0, EvenNumbersCalculator.add(new int[] {}));
	}
	@Test
	public final void evenNumberReturnsItself() {
		Assert.assertEquals(2, EvenNumbersCalculator.add(new int[] {2}));
	}
	@Test
	public final void omitNegativeNumber() {
		Assert.assertEquals(0, EvenNumbersCalculator.add(new int[] {-260}));
	}
	@Test
	public final void omitOddNumber() {
		Assert.assertEquals(0, EvenNumbersCalculator.add(new int[] {1337}));
	}
	@Test
	public final void evenNumbersReturnTheirSum() {
		Assert.assertEquals(2+4+6, EvenNumbersCalculator.add(new int[] {2, 4 ,6}));
	}
	@Test
	public final void omitNegativeNumbers() {
		Assert.assertEquals(2+4+6, EvenNumbersCalculator.add(new int[] {-2, -4, 2, 4 ,6}));
	}
	@Test
	public final void omitOddNumbers() {
		Assert.assertEquals(2+4+6, EvenNumbersCalculator.add(new int[] {1, 2, 4 ,6}));
	}
    @Test
    public final void omitNegativeAndOddNumbers() {
        Assert.assertEquals(2+4+6, EvenNumbersCalculator.add(new int[] {-1, -2, 1, 2, 4 ,6}));
    }
}
